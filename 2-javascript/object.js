const volume = 2;
const book = {
  title: `Csináld magad, ${volume}. kötet`,
  author: { name: 'John Smith' },
  show(length = 10) {
    alert(`${this.title}, ${this.author.name} konyve 
          n${'a'.repeat(length)}gyon unalmas`);
  },
};

window.onload = () => {
  document.getElementById('button').addEventListener('click', book.show(5));
};
