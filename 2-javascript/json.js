// objektum konstruktor
function Person(firstName, lastName, age) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.age = age;
}

const people = [
  new Person('firstName1', 'lastName1', 42),
  new Person('firstName2', 'lastName2', 18),
  new Person('firstName3', 'lastName3', 66),
];

function writeTable() {
  let content = `
          <table border="1">
            <tr>
              <th>First name</th>
              <th>Last name</th>
              <th>Age</th>
            </tr>
        `;
  people.forEach((person) => {
    console.log(person);
    content += `
            <tr>
              <td>${person.firstName}</td>
              <td>${person.lastName}</td>
              <td>${person.age}</td>
            </tr>
          `;
  });
  content += '</table>';

  document.getElementById('tablecontent').innerHTML = content;
}

function writeJson() {
  const jsonContent = JSON.stringify(people);
  document.getElementById('jsoncontent').value = jsonContent;
}

window.onload = () => {
  writeTable();
  writeJson();
};
