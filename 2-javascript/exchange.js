function exchange(identifier) {
  // a p elemnek egy gyermek node-ja (a text node) lesz
  // ennek módosítsjuk értékét
  document.getElementById(identifier).childNodes[0].nodeValue = 'kicserélt szöveg';
}

function exchangeColor(identifier) {
  // CSS stílus fölülírás
  document.getElementById(identifier).style.color = 'red';
}

window.onload = () => {
  document.getElementById('textExchange').addEventListener('click', () => exchange('para1'));
  document.getElementById('colorExchange').addEventListener('click', () => exchangeColor('para1'));
};
