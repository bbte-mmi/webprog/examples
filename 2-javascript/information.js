function printInfo() {
  document.writeln(`
    <em>Dokumentum infó:</em>
    <ul>
      <li><b>URL:</b> ${window.location}</li>
      <li><b>Módosítás dátuma:</b> ${document.lastModified}</li>
      <li><b>Cím:</b> ${document.title}</li>
    </ul>
    <em>Böngésző infó:</em>
    <ul>
      <li><b>UserAgent:</b> ${navigator.userAgent}</li>
    </ul>
    <em>Képernyő infó:</em>
    <ul>
      <li><b>Szélessége:</b> ${window.screen.width}</li>
      <li><b>Magassága:</b> ${window.screen.height}</li>
    </ul>
  `);
}

window.onload = printInfo;
