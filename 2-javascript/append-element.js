function insert() {
  const container = document.getElementById('container');
  const element = document.createElement('div');
  element.innerText = 'Ezt szurtam be';
  element.className = 'inserted';
  container.appendChild(element);
}

function transition(node) {
  node.style.width = '250px';
}

window.onload = () => {
  document.getElementById('insertButton').addEventListener('click', insert);
  document
    .getElementById('transitionButton')
    .addEventListener('click', () => transition(document.getElementById('container').lastChild));
};
