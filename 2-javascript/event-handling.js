function handleButtonPress() {
  console.log('handleButtonPress');
}

window.onload = () => {
  // a button id-val rendelkező elem click eseményéhez hozzárendeljük a "handleButtonPress2" függvényt
  document.getElementById('button').addEventListener('click', handleButtonPress);
};
