function getMessages() {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      // JSON dekódolása a válaszból
      const messages = JSON.parse(xhr.responseText);
      // kirajzolás
      document.getElementById('messages').value = JSON.stringify(messages, 0, 2);
    }
  };

  xhr.open('GET', '/messages');
  xhr.send();
}

function sendMessage() {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      alert(xhr.responseText);
      getMessages();
    }
  };

  xhr.open('POST', '/messages');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(
    JSON.stringify({
      text: document.getElementById('newMessage').value,
      date: new Date(),
    }),
  );
}

window.onload = () => {
  document.getElementById('sendMessage').addEventListener('click', sendMessage);
  getMessages();
};
