async function getMessage() {
  // aszinkron HTTP kérés
  const response = await fetch('/message');
  // body dekódolása
  const message = await response.text();
  // szöveg használata
  document.getElementById('message').innerText = message;
}

window.onload = () => {
  document.getElementById('getMessage').addEventListener('click', getMessage);
};
