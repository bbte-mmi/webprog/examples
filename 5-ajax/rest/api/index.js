// Ez az állomány egy REST API gyökerének express routerét konfigurálja.
// Betölti a további API endpoint lekezelőket.
import express from 'express';
import morgan from 'morgan';
import fullUrlMiddleware from '../middleware/fullurl.js';
import blogPostRoutes from './blogPosts.js';
import userRoutes from './users.js';

const router = new express.Router();

// loggoljuk csak az API kéréseket
router.use(morgan('tiny'));

// tároljuk minden hívás teljes URL-jét
// a HATEOAS kialakításáért
router.use(fullUrlMiddleware);

// minden testtel ellátott API hívás
// JSON-t tartalmaz
router.use(express.json());

// API endpointok a megfelelő alrouterbe
router.use('/blogPosts', blogPostRoutes);
router.use('/users', userRoutes);

export default router;
