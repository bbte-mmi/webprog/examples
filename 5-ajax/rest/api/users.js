import express from 'express';
import * as blogPostDao from '../db/blogPosts.js';
import * as userDao from '../db/users.js';
import hasProps from '../middleware/validate.js';

const router = new express.Router();

// findAll
router.get('/', async (req, res) => {
  try {
    const users = await userDao.findAllUsers();
    res.json(users);
  } catch (err) {
    res.status(500).json({ message: `Error while finding all users: ${err.message}` });
  }
});

// findById
router.get('/:userId', async (req, res) => {
  const { userId } = req.params;
  try {
    const user = await userDao.findUserById(userId);
    if (!user) {
      res.status(404).json({ message: `User with ID ${userId} not found.` });
      return;
    }
    res.json(user);
  } catch (err) {
    res.status(500).json({ message: `Error while finding user with ID ${userId}: ${err.message}` });
  }
});

// findBlogPostsForUser
router.get('/:userId/blogPosts', async (req, res) => {
  const { userId } = req.params;

  try {
    const exists = await userDao.userExists(userId);
    if (!exists) {
      res.status(404).json({ message: `User with ID ${userId} not found.` });
      return;
    }
    const blogPosts = await blogPostDao.findBlogPostsByAuthorId(userId);
    res.json(blogPosts);
  } catch (err) {
    res.status(500).json({ message: `Error while finding blog posts for user with ID ${userId}: ${err.message}` });
  }
});

// insert
router.post('/', hasProps(['fullName']), async (req, res) => {
  try {
    const user = await userDao.insertUser(req.body);
    res.status(201).location(`${req.fullUrl}/${user.id}`).json(user);
  } catch (err) {
    res.status(500).json({ message: `Error while creating user: ${err.message}` });
  }
});

// delete
router.delete('/:userId', async (req, res) => {
  const { userId } = req.params;

  try {
    const exists = await userDao.deleteUser(userId);
    if (!exists) {
      res.status(404).json({ message: `User with ID ${userId} not found.` });
      return;
    }
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({ message: `Error while deleting user with ID ${userId}: ${err.message}` });
  }
});

// update puttal
router.put('/:userId', hasProps(['fullName']), async (req, res) => {
  const { userId } = req.params;

  try {
    await userDao.updateOrInsertUser(userId, req.body);
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({ message: `Error while updating user with ID ${userId}: ${err.message}` });
  }
});

// update patch-csel
router.patch('/:userId', async (req, res) => {
  const { userId } = req.params;

  try {
    const exists = await userDao.updateUser(userId, req.body);
    if (!exists) {
      res.status(404).json({ message: `User with ID ${userId} not found.` });
      return;
    }
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({ message: `Error while updating user with ID ${userId}: ${err.message}` });
  }
});

export default router;
