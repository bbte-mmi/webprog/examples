/*
Form adatok lekezelését végzi.
A statikus mappában adott egy HTML egy formmal - ezt szolgáljuk fel GET híváskor a gyökérre
Az ottani form leadását feldolgozzuk itt express segítségével.

Ez egy kiegészítése a 3-nodejs formhandling példának ASZINKRON hívások küldésére.
*/

import express from 'express';
import path from 'path';
import multer from 'multer';

const app = express();

// a static mappából adjuk a HTML állományokat
app.use(express.static(path.join(process.cwd(), 'static')));

// formfeldolgozás
// multert használunk, de nem fogadunk file-okat vele
app.post('/submit_form', multer().none(), (request, response) => {
  const message = `A szerver sikeresen megkapta a következő információt:
    név: ${request.body.name}
    jelszó: ${request.body.password} (sosem szabad ezt így kiírni :))
    születési dátum: ${new Date(request.body.birthdate).toLocaleDateString()}
    nem: ${request.body.gender}
    tud HTML-t: ${Boolean(request.body.knowshtml)}
  `;
  console.log(message);

  response.set('Content-Type', 'text/plain;charset=utf-8');
  response.end(message);
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
