import express from 'express';
import session from 'express-session';

// ezek az értékek adatbázisból jöjjenek
const users = {
  egyuser: {
    password: 'egyjelszo',
    role: 'user',
  },
  masikuser: {
    password: 'masikjelszo',
    role: 'admin',
  },
};

const app = express();

// session middleware beállítása
app.use(
  session({
    secret: '142e6ecf42884f03',
    resave: false,
    saveUninitialized: true,
  }),
);
app.use(express.urlencoded({ extended: true }));

// belépés
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  if (username && password && users[username] && users[username].password === password) {
    req.session.username = username;
    req.session.role = users[username].role;
    res.send('Login successful');
  } else {
    res.status(401).send('Wrong credentials');
  }
});

// kilépés
app.post('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send(`Session reset error: ${err.message}`);
    } else {
      res.send('Logout successful');
    }
  });
});

// middleware az engedélyezéshez
function authorize(roles = ['user', 'admin']) {
  return (req, res, next) => {
    if (!req.session.role) {
      // a felhasználó nincs bejelentkezve
      res.status(401).send('You are not logged in');
    } else if (!roles.includes(req.session.role)) {
      // a felhasználó be van jelentkezve de nincs joga ehhez az operációhoz
      res.status(403).send('You do not have permission to access this endpoint');
    } else {
      // minden rendben
      next();
    }
  };
}

app.get('/restricted', authorize(), (req, res) => {
  res.send('Congrats, you are in an area only for users');
});

app.get('/restricted_admins', authorize(['admin']), (req, res) => {
  res.send('Congrats, you are in an area only for admins');
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
