/* Egyszerű példa egy HTTP szerverre amely három elérési útra válaszol:
1. /setcookie - válaszban kéri a böngészőtől egy süti beállítását
2. /getcookies - kiirja console-ra a kérés fejlécét
3. /deletecookie - utasítja a klienst a süti törlésére

felhasznált modulok:
  express: web szerver
  cookie-parser: feldolgozza a kérés fejlécben érkező cookie-kat
                és elérhetővé teszi a request.cookie objektumon keresztül
*/
import express from 'express';
import cookieParser from 'cookie-parser';

const app = express();

// sütiket feldolgozó middleware HTTP hívásokból
// beállítja a req.cookies adattagot
app.use(cookieParser());

// beállít egy sütit
app.get('/setcookie', (req, res) => {
  console.log("Setting 'mycookie' cookie on the client");
  // a válaszban utasítja a klienst, hogy mentse el a megadott cookie-t
  res.cookie('mycookie', 'mycookievalue', {
    maxAge: 30000, // 30 percig érvényes
  });
  res.send("Received command to set the 'mycookie' cookie");
});

// kiírja az aktív sütiket
app.get('/getcookies', (req, res) => {
  let msg = 'Received the following cookies:\n';
  msg += Object.entries(req.cookies)
    .map(([cookieName, cookieValue]) => `  ${cookieName} : ${cookieValue}`)
    .join('\n');
  console.log(msg);
  res.setHeader('Content-Type', 'text/plain');
  res.send(msg);
});

// törli a fent megadott sütit
app.get('/deletecookie', (req, res) => {
  console.log('Deleting cookie');
  res.clearCookie('mycookie');
  res.send('Cookie cleaned');
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
