/*
Ez a példa a HTTP Basic hitelesítési mechanizmusát demózza
*/
import express from 'express';

// ezek az értékek adatbázisból jöjjenek
const users = {
  egyuser: 'egyjelszo',
  masikuser: 'masikjelszo',
};

const app = express();

// HTTP basic auth middleware
app.use((req, res, next) => {
  const auth = req.headers.authorization;
  if (auth) {
    // kibányásszuk a bejelentkezési infót a headerekből
    const b64auth = auth.split(' ')[1];
    const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':');

    // megnézzük a helyességüket
    if (users[username] === password) {
      // Access granted...
      next();
      return;
    }
  }

  // Access denied...
  res.set('WWW-Authenticate', 'Basic realm="WebProg példa"');
  res.status(401).send('Authentication required');
});

// Minden hívás bejelentkezést kényszerít
app.get('/', (req, res) => {
  res.send('Welcome, authenticated user!');
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
