/* Példaprogram, amely egy session változóban számolja, hogy
egy adott felhasználó hányszor látogatott meg bizonyos oldalakat

Figyelem: ez a példa az alapértelmezett (memóriában való) sesszió
tárolási mechanizmust használja, ami csak fejlesztés során hasznlható.
A példa az express-session github project oldaláról származik.
*/
import express from 'express';
import session from 'express-session';

const app = express();

// session middleware beállítása
app.use(
  session({
    // aláírjuk a session ID sütit
    secret: '142e6ecf42884f03',
    // mentjük-e a sessiont ha nem is változott
    resave: false,
    // mentünk-e sessiont ha nem írtunk bele semmit
    saveUninitialized: false,
  }),
);

// egyszerű elérés-számláló
app.get('/count', (req, res) => {
  // inicializálása, ha ez még nem történt meg + növelés
  req.session.count = (req.session.count || 0) + 1;
  // elérjük a req.session-ben az ID-t is
  console.log(`Session ID: ${req.session.id}, view count: ${req.session.count}`);
  res.send(`YOU viewed this page ${req.session.count} times`);
});

// újraindítás (session törlése)
// megfelel egy kijelentkezésnek
app.post('/reset_session', (req, res) => {
  console.log(`Logging out (destroying data) of ${req.session.id}`);
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send(`Session reset error: ${err.message}`);
    } else {
      res.send('Your session has been destroyed');
    }
  });
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
