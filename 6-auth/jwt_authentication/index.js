import express from 'express';
import jwt from 'jsonwebtoken';

// ezek az értékek adatbázisból jöjjenek
const users = {
  egyuser: 'egyjelszo',
  masikuser: 'masikjelszo',
};

const secret = '1c28d07215544bd1b24faccad6c14a04';

const app = express();

// belépés
app.post('/login', express.urlencoded({ extended: true }), (req, res) => {
  const { username, password } = req.body;

  if (users[username] === password) {
    // sikeres belépés, JWT generálása
    const token = jwt.sign({ username }, secret, {
      expiresIn: '1h',
    });
    res.send(`Log in successful, your JWT token is: ${token}`);
  } else {
    res.status(401).send('Username/password combination incorrect.');
  }
});

// erőforrás, melyet csak valid JWT tokennel érünk el
app.get('/restricted', (req, res) => {
  const auth = req.headers.authorization;
  if (auth) {
    // kibányásszuk a JWT tokent a headerből
    const token = auth.split(' ')[1];
    // verifikáljuk az aláírást
    jwt.verify(token, secret, (err, payload) => {
      if (payload) {
        console.log(`JWT successfully verified for ${payload.username}`);
        res.send('Congratulations, your JWT token is valid');
      } else {
        res.status(401).send('You are not logged in properly');
      }
    });
  } else {
    res.status(401).send('You are not logged in properly');
  }
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
