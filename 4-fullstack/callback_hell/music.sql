-- Segédállomány, hogy előkészítsünk egy MySQL adatbázist a példaprogramnak.
-- Futtatás konzolról (UNIX rendszeren): 
--     mysql -u root -p <music.sql

CREATE DATABASE IF NOT EXISTS music;
USE music;
GRANT ALL PRIVILEGES ON *.* TO 'webprog'@'%' IDENTIFIED BY 'VgJUjBd8';

DROP TABLE IF EXISTS `artist`;
DROP TABLE IF EXISTS `album`;
DROP TABLE IF EXISTS `track`;

-- táblák

CREATE TABLE `artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45),
  `artist_id` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45),
  `album_id` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


-- tesztadat

INSERT INTO `artist` VALUES
  (default,"Linkin Park"),
  (default,"Metallica"),
  (default,"Rammstein");

INSERT INTO `album` VALUES 
  (default,"Hybrid Theory",1),
  (default,"Meteora",1),
  (default,"Minutes to Midnight",1),
  (default,"Kill 'Em All",2),
  (default,"Ride the Lightning",2),
  (default,"Herzeleid",3),
  (default,"Sehnsucht",3);

INSERT INTO `track` VALUES 
  (default,"Papercut",1),
  (default,"One Step Closer",1),
  (default,"Don't Stay",2),
  (default,"Somewhere I Belong",2),
  (default,"Numb",2),
  (default,"What I've Done",3),
  (default,"Shadow of the Day",3),
  (default,"Jump in the Fire",4),
  (default,"Motorbreath",4),
  (default,"Fade to Black",5),
  (default,"For Whom the Bell Tolls",5),
  (default,"Du riechst so gut",6),
  (default,"Rammstein",6),
  (default,"Engel",7),
  (default,"Du hast",7);
