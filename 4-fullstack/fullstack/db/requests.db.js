import mysql from 'mysql2/promise.js';

/**
 * Adatelérési réteget jelképező osztály
 */
export class RequestRepo {
  constructor() {
    this.pool = mysql.createPool({
      connectionLimit: 10,
      database: 'webprog',
      host: 'localhost',
      port: 3306,
      user: 'webprog',
      password: 'VgJUjBd8',
    });
  }

  async createTables() {
    await this.pool.query(`CREATE TABLE IF NOT EXISTS requests (
      method varchar(20),
      url varchar(50),
      date datetime);
    `);
    console.log('Table created successfully');
  }

  // lekéri az összes sort
  // Promise-t térít vissza
  findAllRequests() {
    const query = 'SELECT * FROM requests';
    return this.pool.query(query);
  }

  // beszúr egy DB sort
  insertRequest(req) {
    const date = new Date();
    const query = 'INSERT INTO requests VALUES (?, ?, ?)';
    return this.pool.query(query, [req.method, req.url, date]);
  }

  // törli az összes sort
  // visszaadja a sikerességet törölt sorok alapján
  async deleteAllRequests() {
    const query = 'DELETE FROM requests';
    const [result] = await this.pool.query(query);
    return result.affectedRows > 0;
  }
}

// alapértelmezett példány exportra
const db = new RequestRepo();

/**
 * Aszinkron létrehozása a táblázatnak.
 * Async/await notációt használ.
 * Top-level async/await elég új a node.js-ben
 */
try {
  await db.createTables();
} catch (err) {
  console.error(`Create table error: ${err}`);
  process.exit(1);
}

export default db;
