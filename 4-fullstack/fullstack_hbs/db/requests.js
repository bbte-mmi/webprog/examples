// Adatbázis műveleteket végző modul
import pool from './connection.js';

/**
 * Aszinkron létrehozása a táblázatnak.
 * Async/await notációt használ.
 * Top-level async/await elég új a node.js-ben
 */
try {
  await pool.query(`CREATE TABLE IF NOT EXISTS requests (
      method varchar(20),
      url varchar(50),
      date datetime);
    `);
  console.log('Table created successfully');
} catch (err) {
  console.error(`Create table error: ${err}`);
  process.exit(1);
}

// lekéri az összes sort
// Promise-t térít vissza
export const findAllRequests = () => {
  const query = 'SELECT * FROM requests';
  return pool.query(query);
};

// beszúr egy DB sort
export const insertRequest = (req) => {
  const query = 'INSERT INTO requests VALUES (?, ?, ?)';
  const date = new Date();
  return pool.query(query, [req.method, req.url, date]);
};

// törli az összes sort
export const deleteAllRequests = () => {
  const query = 'DELETE FROM requests';
  return pool.query(query);
};
