/*
Csatlakozik egy MS SQL adatbázishoz.
Minden HTTP kérés érkezése esetén beírja az adatbázisba
a kérés metódusát, az URL-t és a kérés időpontját.

Figyelem! npm i csak az MSSQL-hez csatlakoztató segédmodult telepíti, nem magát a MSSQL-t.
*/
import express from 'express';
import sql from 'mssql';

// létesít egy connection poolt az adatbázissal
// a megadott DB-nek/felhasználónak léteznie kell
// létre lehet hozni a mellékelt setup.sql szkript segítségével
const pool = await sql.connect({
  server: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'webprog',
  options: {
    trustServerCertificate: true,
  },
});

// végrehajt egy SQL parancsot
// létrehozzunk a táblázatot, ha még nem létezik
await sql.query(
  `IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='requests' and xtype='U')
    CREATE TABLE requests (
      method varchar(20),
      url varchar(50),
      date datetime)`,
);
console.log('Table exists successfully');

const app = express();

// DB-tartalom visszatérítése
app.get('/requests', async (req, res) => {
  try {
    const result = await sql.query('SELECT * FROM requests');
    // recordsets alatt a válaszsorok JS objektumként
    res.send(result.recordsets);
  } catch (err) {
    const msg = `Selection unsuccessful: ${err}`;
    console.error(msg);
    res.status(500).send(msg);
  }
});

// minden hívás kerüljön be DB-be
app.use(async (req, res) => {
  // felépítjük a végrehajtandó SQL lekérdezést
  // prepared statement - @ helytartók a paramétereknek
  const query = 'INSERT INTO requests (method, url, date) VALUES (@method, @url, @date)';
  console.log(`Executing query ${query}`);

  // végrehatjuk a beszúrást
  try {
    const result = await pool
      .request()
      .input('method', req.method)
      .input('url', req.url)
      .input('date', new Date())
      .query(query);
    res.send(`Successfully inserted ${result.rowsAffected} rows`);
  } catch (err) {
    const msg = `Insertion unsuccessful: ${err}`;
    console.error(msg);
    res.status(500).send(msg);
  }
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
