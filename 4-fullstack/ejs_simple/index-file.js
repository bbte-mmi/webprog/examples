// példa a ejs használatára
import ejs from 'ejs';

// megadunk egy modellt (dinamikus tartalom)
const model = {
  name: 'Alan',
  hometown: 'Somewhere, TX',
  kids: [{ name: 'Jimmy', age: '12' }, { name: 'Sally', age: '4' }, { name: 'Johnny' }],
};

// a sablon renderelése EJS állomány alapján
ejs.renderFile('example.ejs', model, (err, view) => {
  if (err) {
    throw err;
  }
  console.log(view);
});
