/*
Csatlakozik egy MySQL adatbázishoz egy connection pool segítségével.
Minden HTTP kérés érkezése esetén beírja az adatbázisba
a kérés metódusát, az URL-t és a kérés időpontját.

Figyelem! npm i csak a MySQL-hez csatlakoztató segédmodult telepíti, nem magát a MySQL-t.
*/
import express from 'express';
import mysql from 'mysql2';

// Létrehozunk egy connection poolt
// a megadott DB-nek/felhasználónak léteznie kell
// létre lehet hozni a mellékelt setup.sql szkript segítségével
const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

// létrehozzunk a táblázatot, ha még nem létezik
// a pool.query kér egy kapcsolatot a poolból
pool.query(
  `
  CREATE TABLE IF NOT EXISTS requests (
    method varchar(20),
    url varchar(50),
    date datetime
  );
  `,
  (createErr) => {
    if (createErr) {
      console.error(`Create table error: ${createErr.message}`);
      process.exit(1);
    } else {
      console.log('Table created successfully');
    }
  },
);

const app = express();

// DB-tartalom visszatérítése
app.get('/requests', (req, res) => {
  // kérünk egy kapcsolatot a pooltól
  pool.getConnection((err, connection) => {
    if (err) {
      connection.release(); // Fontos visszaadni itt
      res.status(500).send(`Error in DB connection: ${err.message}`);
      return;
    }

    // query esetén a callback 2. paramétere
    // a visszatérített információ
    connection.query('SELECT * FROM requests', (selectErr, result) => {
      if (selectErr) {
        const msg = `Selection failed: ${selectErr}`;
        res.status(500).send(msg);
      } else {
        // adatbázis válasz visszaküldése JSON formájában
        res.send(result);
      }
      // visszahelyezi a kapcsolatobjektumot a készletbe, így az ismét használható mások által is
      connection.release();
    });
  });
});

// minden hívás kerüljön be DB-be
app.use((req, res) => {
  const { method, url } = req;
  const date = new Date().toLocaleString('sv');

  // felépítjük a végrehajtandó SQL lekérdezést
  const queryString = `INSERT INTO requests VALUES ("${method}", "${url}", "${date}");`;
  console.log(`Executing query ${queryString}`);

  // végrehajtjuk a beszúrást
  pool.query(queryString, (insertErr, result) => {
    if (insertErr) {
      const msg = `Insertion unsuccessful: ${insertErr}`;
      res.status(500).send(msg);
      return;
    }
    res.send(`Successfully inserted ${result.affectedRows} rows`);
  });
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
