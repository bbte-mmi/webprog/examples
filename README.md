# Webprogramozás példaprogramok

A webprogramozás tantárgy laborfeladatainak sikeres megoldásához szükségünk lesz különböző szoftver eszközökre, amelyeket otthoni használat céljából telepítenünk kell a saját számítógépünkre. A legfontosabb eszközök listája a következő:

1. Egy modern böngésző. legfrisebb verzión: [Chrome](https://www.google.com/chrome/), [Firefox](https://www.mozilla.org/en-US/firefox), [Safari](https://safari.en.softonic.com/), stb.
2. Szintaxis kiemeléssel (syntax highlighting) rendelkező szövegszerkesztő: ajánlott a [Visual Studio Code](https://code.visualstudio.com/).
3. [node.js](https://nodejs.org/en/) JavaScript runtime
    - ajánlott a legfrissebb *LTS* (long term support) verzió használata
    - kipróbálható konzolról a `node`, `npm` és `npx` parancsok segítségével 
4. [git verziókövető](https://git-scm.com). Használati opciók:
    - konzolból a velejáró *Git Bash* segítségével;
    - a szerkesztő pluginja segítségével - [git VS Code-ból](https://code.visualstudio.com/docs/editor/versioncontrol#_git-support);
5. Teszteszköz HTTP hívásoknak, pl. [Postman](https://www.getpostman.com/downloads/), [bruno](https://github.com/usebruno/bruno), [Thunder Client](https://www.thunderclient.com/)
6. Valamilyen adatbáziskezelő rendszer (lehet relációs vagy NoSQL is; **későbbi feladatoknál szükséges**), pl. [MariaDB](https://mariadb.com/downloads/), [PostgreSQL](https://www.postgresql.org/download/), [MySQL](https://dev.mysql.com/downloads/mysql/) vagy [MongoDB](https://www.mongodb.com/try/download/community)
    - alkalmazható egy online, cloudban futó változat is, pl. [MongoDB Atlas](https://www.mongodb.com/atlas/database) vagy [SkySQL](https://mariadb.com/products/skysql/) (MariaDB/MySQL as a Service)
    - [Docker](https://docs.docker.com/get-started/overview/) és [Docker Compose](https://docs.docker.com/get-started/08_using_compose/) ismeretében használhatóak a telepítések a [példaprogramos repo `tools` mappájában](https://gitlab.com/bbte-mmi/webprog/examples/-/tree/master/tools) is.


# Példaprogramok

## Szerkezet

- A példaprogramok tematika szerint vannak csoportosítva. Minden tematikának egy külön könyvtár felel meg, ezen belül megtalálhatóak az aktuális példaprogramok.

## Használat

### Első letöltés

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova letöltenénk a példákat: `cd <celpont>`
    - letöltés: `git clone https://gitlab.com/bbte-mmi/webprog/examples.git` (létrehoz egy `examples` almappát)
- *VS Code*-ból:
    - Command pallette: `Ctrl+Shift+P`
    - "Git: Clone"
    - Repo URL: `https://gitlab.com/bbte-mmi/webprog/examples.git` (ha nem működik, írd újra a `-` jelet)

### A tartalom frissítése

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova korábban letöltöttük a példákat: `cd <celpont>/examples`
    - frissítés: `git pull`
- *VS Code*-ból:  
![](vscode-sync.png){width="40%"}

### Hiba esetén

Hiba léphet fel, hogyha a lokális tárolóban levő tartalom *megváltozott* (ha kézzel változtattok a példaprogramok tartalmán). A könyvtár *tisztításáért* hívjátok a következőt ugyanazon `examples` mappából (**Vigyázat!:** Ezen parancsok meghívása a laborfeladatos tárolóban kitörölheti annak módosításait):  
```
    git reset --hard HEAD
    git clean -xfd
```

# Saját laborfeladatos tároló

- A laborfeladatokat a GitLab verziókövető portálra kell feltölteni. Minden diáknak automatikusan készül egy-egy projekt, melynek URL-je: `https://gitlab.com/bbte-mmi/webprog/labs/<év>/<abcd1234>`  
  ahol az `abcd1234` megfelel a felhaszálói SSID-toknak. Ide töltsétek fel gittel a `develop` ágra (ez lesz használva automatikusan) minden megoldást.
- Ahhoz hogy a projekthez elérést nyerjetek, [készítsetek felhaszálót a GitLabon](https://gitlab.com/users/sign_in#register-pane), majd adjátok le az itt használt **felhasználótokat** itt: <https://bit.ly/webprog-gitlab>
- A laborfeladatokhoz tartozik egy *merge request* (automatikusan létrehozva általunk). A feladatokat **inkrementálisan** ajánljuk megoldani a kurzusok mentén. A találkozási alkalmak között opcionálisan lehet kérni review-t az aktuális tartalomra.
- Az értékelésnél kizárólag a GitLabra felpusholt állományokat vesszük figyelembe. A leadás dátumának az *utolsó commit* időpontja számít.


# *Bónusz:* SSH használata gittel

Annak érdekében, hogy ne kelljen minden pull/push operációkor *felhasználót és jelszót* megadj a gitnek, konfigurálható az SSH-s elérés. Ennek lépései:

1. Lokálisan a *Git Bash*-ből: `ssh-keygen` - minden kért argumentumot **üresen hagyva**.
2. `cat ~/.ssh/id_rsa.pub` - ez kiírja a publikus SSH kulcsot (figyeld az előző parancs kimenetét, azt a fájlt kell megnyitni, ami `id`-vel kezdődik és `.pub`-ra végződik, előfordulhat, hogy nem `rsa` lesz, hanem például `ed22519`).
3. GitLab-on a profilodon a *Preferences* -> *SSH Keys* részbe másold be az SSH kulcsot *teljességében* (`ssh-rsa` kulcsszó az elején, a géped neve a végén).
4. A projekt oldalán másold le az SSH-s elérési linket - ennek formája `git@gitlab.com:bbte-mmi/webprog/...`
5. A fenti HTTP-s `clone` parancs helyett: `git clone <link>` Ha már klónoztad a repót korábban, akkor annak folderén belül: `git remote set-url origin <link>`
