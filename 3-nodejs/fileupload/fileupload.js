/*
File-feltöltést engedélyez HTML formon keresztül.
A feltöltött állományokat elmenti egy almappába
*/

import express from 'express';
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import multer from 'multer';

const app = express();

// a static mappából adjuk a HTML állományokat
app.use(express.static(join(process.cwd(), 'static')));

// feltöltési mappa elkészítése
const uploadDir = join(process.cwd(), 'uploadDir');
if (!existsSync(uploadDir)) {
  mkdirSync(uploadDir);
}

// multer middleware előkészítése
const multerUpload = multer({
  dest: uploadDir,
  limits: {
    fileSize: 2 * 1024 * 1024, // 2 MB-s méret limit
  },
});

// formfeldolgozás multer middleware-rel
// megadjuk a field-et ahol file-ra számítunk
app.post('/upload_file', multerUpload.single('myfile'), (request, response) => {
  // az állomány(ok) a request.file(s)-ban lesznek
  const fileHandler = request.file;
  // más mezők a request.body-ben
  const privateFile = Boolean(request.body.private);

  const message = `Feltöltés érkezett:
    állománynév: ${fileHandler.originalname}
    név a szerveren: ${fileHandler.path}
    méret: ${fileHandler.size}
    mime-type: ${fileHandler.mimetype}
    privát (body): ${privateFile}
  `;

  console.log(message);
  response.set('Content-Type', 'text/plain;charset=utf-8');
  response.end(message);
});

app.listen(8080, () => {
  console.log('Server listening...');
});
