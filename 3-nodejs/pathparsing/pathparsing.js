// Egyszerű példa az url modul használatára a kérés URL felbontására.

import { createServer } from 'http';
import { URL, URLSearchParams } from 'url';

function showParsedUrl(urlToParse) {
  const parsedUrl = new URL(urlToParse, 'http://localhost:8080/');
  const queryParams = new URLSearchParams(parsedUrl.search);

  console.log(`URL:
    href: ${parsedUrl.href}
    protocol: ${parsedUrl.protocol}
    hostname: ${parsedUrl.hostname}
    port: ${parsedUrl.port}
    pathname: ${parsedUrl.pathname}
    query: ${queryParams.toString()}
  `);
}

const server = createServer((request, response) => {
  const body = [];

  request.on('data', (chunk) => body.push(chunk));
  request.on('error', (err) => console.error(err));

  request.on('end', () => {
    showParsedUrl(request.url);
    response.end('OK');
  });
});

// Próba teljes URL-re
showParsedUrl('http://localhost:8080/myPath?param1=value1&param2=value2');

server.listen(8080, () => {
  console.log('Server listening...');
});

// Futtatás: node pathparsing.js
