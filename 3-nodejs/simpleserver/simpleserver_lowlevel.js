/*
Egyszerű példa egy HTTP szerverre amely minden HTTP kérés esetén kiírja
a console-ra a kérés metódusát, az URL-t, valamint a kérés törzsében
levő adatokat string formájában (ha vannak adatok).

Válaszként csak egy 200-as státuszkódot küld vissza.

Tesztelni lehet:
- böngészőből: http://localhost:8080/tetszoleges_eleresi_ut_es_query_string
- Postmannel, curl-lel
*/

import { createServer } from 'http';

const server = createServer((request, response) => {
  let body = [];

  request.on('data', (chunk) => body.push(chunk));
  request.on('error', (err) => console.error(err));

  request.on('end', () => {
    body = Buffer.concat(body).toString();

    const info = `You have requested:
      - method: ${request.method}
      - URL: ${request.url}
      - headers (as JSON): ${JSON.stringify(request.headers)}
      - body: ${body}
    `;
    console.log(info);

    // megadjuk a státuszkódot és fejlécinfót
    response.writeHead(200, {
      'Content-Type': 'text/plain; charset=utf-8',
    });
    // befejezi a választ
    // és visszaküldi a kliensnek
    response.end(info);
  });
});

// A listen metódus hívásának hatására aktiválódik
// a szerver és fogadja a 8080-as porton
// beérkező HTTP kéréseket
server.listen(8080, () => {
  console.log('Server listening...');
});

// Futtatás: node simpleserver_lowlevel.js
