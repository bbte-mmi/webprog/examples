/* JSON adatok kezelése */

import express from 'express';

const app = express();

// JSON formátumú body dekódolása és feldolgozása
app.post('/submit_json', express.json(), (request, response) => {
  console.log('A szerver sikeresen megkapta a következő információt JSON formátumban');
  console.log(request.body);

  // felépítünk egy komplex választ
  // az express automatikusan JSON-ban küldi vissza
  response.send({
    reqBody: request.body,
    date: new Date(),
  });
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
