// Egyszerű példa egy, az aktuális könyvtárból statikus erőforrásokat kiszolgáló web-szerverre
// amely az express modul-t használja.

import express from 'express';
import morgan from 'morgan';
import { join } from 'path';

// a mappa ahonnan statikus tartalmat szolgálunk
// process.cwd() - globális változó, az aktuális katalógusra mutat a szerveren
// SOSE a gyökeret tegyünk publikussá
const staticDir = join(process.cwd(), 'static');

// inicializáljuk az express alkalmazást
const app = express();

// morgan middleware: loggolja a beérkezett hívásokat
app.use(morgan('tiny'));
// express static middleware: statikus állományokat szolgál fel
app.use(express.static(staticDir));

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});

/*
Futtatás:
  npm i express morgan
  node index.js
*/
